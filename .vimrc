syntax enable

if &compatible
    set nocompatible
end

" some plugins assume this
set shell=bash

" set relativenumber
set number

set expandtab
set tabstop=4
set shiftwidth=4
set smartindent
set breakindent
set backspace=indent,eol,start

set wildmenu
set wildmode=longest,list,full

set showmatch
set incsearch
set hlsearch

set lazyredraw
set scrolloff=4
set ruler
set noerrorbells
set visualbell

set pastetoggle=<f2>
set autoread
set autochdir
set autoread
set autowriteall
set confirm

set t_vb=

nnoremap j gj
nnoremap k gk
nnoremap gV `[v`]

set clipboard=unnamedplus,unnamed

let mapleader=' '
map <leader>fs :w<cr>
map <leader>mm :make<cr>
map <leader><tab> :b!#<cr>
map <leader>bb :b! 
map <leader>bd :bdelete<cr>
map <leader>ff :edit 
map <leader>qq :wq<cr>
map <silent> <leader>/ :nohlsearch<cr>

map <leader>1 :b! 1<cr>
map <leader>2 :b! 2<cr>
map <leader>3 :b! 3<cr>
map <leader>4 :b! 4<cr>
map <leader>5 :b! 5<cr>
map <leader>6 :b! 6<cr>
map <leader>7 :b! 7<cr>
map <leader>8 :b! 8<cr>
map <leader>9 :b! 9<cr>

set rtp+=~/.vim/bundle/repos/github.com/Shougo/dein.vim
cal dein#begin(expand('~/.vim/bundle/'))
cal dein#add('Shougo/dein.vim')
cal dein#add('bling/vim-bufferline')
cal dein#add('tmux-plugins/vim-tmux')
cal dein#add('tpope/vim-commentary')
cal dein#add('tpope/vim-surround')
cal dein#add('editorconfig/editorconfig-vim')
cal dein#add('dag/vim-fish')
"cal dein#add('Raimondi/delimitMate')
cal dein#add('udalov/kotlin-vim')
cal dein#add('embear/vim-localvimrc')
cal dein#add('welle/targets.vim')
call map(dein#check_clean(), "delete(v:val, 'rf')")
cal dein#end()

let g:local_vimrc='.dir-locals.vim'
map <silent> <leader>; :Commentary<cr>
noremap <silent> <leader>d <C-]>
noremap <leader>t :tag 
noremap <silent> <leader>D <C-t>

cmap w!! w !sudo tee % > /dev/null

let g:delimitMate_expand_cr=1
let g:localvimrc_ask=0

filetype indent plugin on

au BufNewFile,BufRead *.jad set ft=java
au BufNewFile,BufRead .jshintrc set ft=json

nnoremap p p`[v`]=
